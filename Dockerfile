# Linux Assignement 5 Docker Image
FROM httpd

# User Information
LABEL username="nininxhel" email="nihel.madani-fouatihnihel@dawsoncollege.qc.ca"

# DocumentRoot
WORKDIR /usr/local/apacha2/htdocs/

# Copying App
COPY app/ /usr/local/apache2/htdocs/

