/* eslint-disable no-console */
"use strict";

const { useState } = React;

function ArtworkList({ data, onArtworkClick }) {
    return (
        <ul onClick={onArtworkClick}>
            {data.map((artwork, index) => (
                <li key={index} data-id={artwork.id}>{artwork.title}</li>
            ))}
        </ul>
    );
}
function removeHtmlTags(html) {
    const doc = new DOMParser().parseFromString(html,'text/html');
    return doc.body.textContent;
}

function App() {
    const [artworkData, setArtworkData] = useState([]);

    const handleButtonClick = (e) => {
        e.preventDefault();

        let artistName = document.querySelector("#search").value;
        let queryParams = { q: `${artistName}` }; 
        const queryParamsString = new URLSearchParams(queryParams).toString();

        fetch(`https://api.artic.edu/api/v1/artworks/search?${queryParamsString}`)
            .then(response => {
                console.log("1) Response object:", response);
                if (!response.ok) {
                    throw new Error("Not 2xx response", { cause: response });
                }
                return response.json();
            })
            .then(obj => {
                console.log("2) Deserialized response:", obj);
                setArtworkData(obj.data);
            })
            .catch(err => {
                console.error("3) Error:", err);
            });
    };

    const handleArtworkClick = (e) => {
        const artworkId = e.target.dataset.id;
    
        fetch(`https://api.artic.edu/api/v1/artworks/${artworkId}?fields=id,,artist_display,title,image_id,description,medium_display,dimensions`)
            .then(response => {
                console.log("1) Response object:", response);
                console.log("hi testing",`https://api.artic.edu/api/v1/artworks/${artworkId}`);
                if (!response.ok) {
                    throw new Error("Not 2xx response", { cause: response });
                }
                return response.json();
            })
            .then(obj => {
                console.log("2) Deserialized response:", obj);
                const imageUrl = `https://www.artic.edu/iiif/2/${obj.data.image_id}/full/843,/0/default.jpg`;
                const title = obj.data.title;
                const description = removeHtmlTags(obj.data.description);
                const artist = obj.data.artist_display;
                const medium = obj.data.medium_display;
                const dimensions = obj.data.dimensions;
        
                document.querySelector("#artwork-image").src = imageUrl;
                document.querySelector(".artist-info").innerText = artist;
                document.querySelector("#title").innerText = title;
                document.querySelector("#text").innerText = description;
                document.querySelector("#medium").innerText = medium;
                document.querySelector("#dimensions").innerText = dimensions;
            })
            .catch(err => {
                console.error("3) Error:", err);
            });
    }
    return (
        <div className="wrapper">
        <header>
            <h1>Art Institute of Chicago Search</h1>
            <form>
                <div>
                    <input type="text" name="search" id="search" />
                    <button onClick={handleButtonClick}>Search</button>
                </div>
            </form>
            <ArtworkList data={artworkData} onArtworkClick={handleArtworkClick} />
            <footer>Assignment 6 by Shakela Hossain</footer>
        </header>
        <main>
            <h2 id="title"></h2>
            <div className="artist-info"></div>
            <img id="artwork-image" alt="Artwork"/>
            <p id="text"></p>
            <dl>
                <dt>Medium:</dt>
                <dd id="medium">description</dd>
            </dl>
            <dl>
                <dt>Dimension</dt>
                <dd id="dimensions">description2</dd>
            </dl>
        </main>
    </div>
    );
}

// #react-root
ReactDOM.render(
    <App />,
    document.querySelector("#react-root")
);
